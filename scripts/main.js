//Setup engine and scene
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
//var keyboard = new THREEx.KeyboardState();
var renderer = new THREE.WebGLRenderer();
renderer.setClearColor(0x919191, 1);
renderer.setSize(window.innerWidth, window.innerHeight);
//var controls = new THREE.OrbitControls(camera);
var loader = new THREE.GLTFLoader();
var clock = new THREE.Clock();
document.body.appendChild(renderer.domElement);

var frameTime;
var objectsToLoad = 0;
var objectsLoaded = 0;
var loadStage = 0;
var loading = true;
var waiting = true;

var commandIn = "";
//5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM

var myName = document.createElement('div');
myName.style.position = 'absolute';
myName.innerHTML = "Jack Sylvester-Barnett";
myName.style.top = 97 + '%';
myName.style.left = 10 + 'px';
document.body.appendChild(myName);

//Load
var Load = function () {
	document.getElementById( "subButton" ).addEventListener( 'click', function () {
		DeleteRobots(scene);
		document.getElementById('outBox').value = " ";
		commandIn = document.getElementById('tBox').value;
		var out = PhaseCommands(commandIn);
		console.log(out);
		for (const a of out) {
			SpawnRobot(loader, scene, a.x, a.y, a.direction, a.command)
		}
		waiting = false;
	}, false );
	LoadWorldObjects(loader, scene);
}

//Update every frame
var Update = function () {
	frameTime = clock.getDelta();
	requestAnimationFrame(Update);
	renderer.render(scene, camera);
	//controls.update();
	if (loading) {
		if (loadStage == 0) {
			//Create the playing grid
			GridStartUp(loader, scene);
			CreateGrid(6, 6);
			
			loadStage = 1;
		}
		else if (loadStage == 1 && gridObjectsToLoad == gridObjectsLoaded) {
			//Set up camera
			//Get center
			console.log(grid);
			var pos1 = GetSquareWorldPos(0, 0);
			var pos2 = GetSquareWorldPos(0, gridSizeY - 1);
			var pos3 = GetSquareWorldPos(gridSizeX - 1, 0);
			var pos4 = GetSquareWorldPos(gridSizeX - 1, gridSizeY - 1);
			var x = (pos1.x + pos2.x, pos3.x, pos4.x) / 4;
			var z = (pos1.z + pos2.z, pos3.z, pos4.z) / 4;
			//controls.target.set(x * 2, 0, z * 2)

			camera.position.set((x * 2) - 15, 10, z * 2);
			camera.lookAt(x * 2, 0, z * 2);
			//Load Lights
			//CreatePointLight(scene, 5, 10, 5, 0xffffff, 1);
			CreatePointLight(scene, 20, 20, 20, 0xfcbe03, 1.5);
			CreatePointLight(scene, -20, 20, -20, 0xffeeba, 1.5);
			loading = false;
		}
	}
	else if (!loading && !waiting){
		RobotUpdate(frameTime);
		document.getElementById('outBox').value = " ";
		for (var r = 0; r < robots.length; r++) {
			if (!robots[r].dead) {
				document.getElementById('outBox').value += robots[r].gridX + " " + robots[r].gridY + " " + robots[r].facing + " ";	
			}
		}
	}
};

Load();
Update();