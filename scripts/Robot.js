var robots = [];
var robotsToLoad = 0;
var robotsLoaded = 0;

const updateSpeed = 10;
var speedCounter = 0;

function SpawnRobot(loader, scene, x, y, facing, commands) {
    robotsToLoad++;
    loader.load('models/robot.glb',
        function (gltf) {
            var robot = {
                gridX: x,
                gridY: y,
                facing: facing,
                commands: commands,
                squencePoint: 0,
                model: gltf.scene,
                dead: false,
                animationEnd: false,
                finished: false,
            };
            var pos = GetSquareWorldPos(robot.gridX, robot.gridY);
            robot.model.position.set(pos.x, pos.y + 1, pos.z);
            SetDirection(robot);
            scene.add(robot.model);
            robots.push(robot);
            robotsLoaded++;
            console.log(robot);
        }
    );
}

function DeleteRobots(scene) {
    for (var r = 0; r < robots.length; r++) {
        scene.remove(robots[r].model);
    }
    robots.length = 0;
}

function SetDirection(obj) {
    obj.model.rotation.set( 0, Math.PI / 2, 0);
    if (obj.facing == "N") { //North
        obj.model.rotation.y += THREE.Math.degToRad(180);
    }
    else if (obj.facing == "W") { //East
        obj.model.rotation.y += THREE.Math.degToRad(-90);
    }
    else if (obj.facing == "S") { //South
        obj.model.rotation.y += THREE.Math.degToRad( 0 );
    }
    else if (obj.facing == "E") { //West
        obj.model.rotation.y += THREE.Math.degToRad(90);
    }
}

function PerformCommand(obj, commandLetter) {
    if (commandLetter == "L") {
        if (obj.facing == "N") { //North
            obj.facing = "W";
        }
        else if (obj.facing == "E") { //East
            obj.facing = "N";
        }
        else if (obj.facing == "S") { //South
            obj.facing = "E";
        }
        else if (obj.facing == "W") { //West
            obj.facing = "S";
        }
        SetDirection(obj);
    }
    else if (commandLetter == "R") {
        if (obj.facing == "N") { //North
            obj.facing = "E";
        }
        else if (obj.facing == "E") { //East
            obj.facing = "S";
        }
        else if (obj.facing == "S") { //South
            obj.facing = "W";
        }
        else if (obj.facing == "W") { //West
            obj.facing = "N";
        }
        SetDirection(obj);
    }
    else if (commandLetter == "M") {
        if (obj.facing == "N") {
            obj.gridY++;
        }
        else if (obj.facing == "E") {
            obj.gridX++;
        }
        else if (obj.facing == "S") {
            obj.gridY--;
        }
        else if (obj.facing == "W") {
            obj.gridX--;
        }
        if (obj.gridX < 0 || obj.gridX > gridSizeX - 1  ||
            obj.gridY < 0 || obj.gridY > gridSizeY - 1 ) {
                obj.dead = true;
                if (obj.facing == "N") {
                    obj.model.position.x += 2.5;
                }
                else if (obj.facing == "E") {
                    obj.model.position.z += 2.5;
                }
                else if (obj.facing == "W") {
                    obj.model.position.z -= 2.5;
                }
                else if (obj.facing == "S") {
                    obj.model.position.x -= 2.5;
                }
        }
        else {
            var pos = GetSquareWorldPos(obj.gridX, obj.gridY);
            obj.model.position.set(pos.x, pos.y + 1, pos.z);
         }
    }
}

function DeathAnimation(obj, frametime) {
    if (obj.facing == "N") {
        obj.model.rotation.y += 2 * frametime;
        obj.model.position.y -= 5 * frametime;
    }
    else if (obj.facing == "E") {
        obj.model.rotation.y += 2 * frametime;
        obj.model.position.y -= 5 * frametime;
    }
    else if (obj.facing == "W") {
        obj.model.rotation.y += 2 * frametime;
        obj.model.position.y -= 5 * frametime;
    }
    else if (obj.facing == "S") {
        obj.model.rotation.y += 2 * frametime;
        obj.model.position.y -= 5 * frametime;
    }

    if (obj.model.position.y < -10) {
        obj.animationEnd = true;
    }
}

function FinishedAnimation(obj, frametime) {
    obj.model.rotation.y += 2 * frametime;
}

function RobotUpdate(frametime) {
    if (speedCounter >= 5) {
        speedCounter = 0;
        for (var r = 0; r < robots.length; r++) {
            if (robots[r].squencePoint < robots[r].commands.length) {
                if (!robots[r].dead) {
                    PerformCommand(robots[r], robots[r].commands[robots[r].squencePoint]);
                    robots[r].squencePoint++;
                }
            }
            else {
                robots[r].finished = true;
            }
        }
    } 
    else {
        for (var r = 0; r < robots.length; r++) {
            if (robots[r].dead && !robots[r].animationEnd) {
                DeathAnimation(robots[r], frametime);
            }
            else if (robots[r].finished) {
                FinishedAnimation(robots[r], frametime);
            }
        }
        speedCounter += updateSpeed * frametime;
    }
}
