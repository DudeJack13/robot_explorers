function CheckIfNumber(str) {
    if (str == "0" || str == "1" || str == "2" || str == "3" || str == "4" || 
    str == "5" || str == "6" || str == "7" || str == "8" || str == "9") {
        return true;
    }
    return false;
}

function CheckIfDirection(str) {
    if (str == "N" || str == "S" || str == "E" || str == "W") {
        return true;
    }
    return false;
}



var PhaseCommands = function (cmdStr) {
    var savedCommands = [];
    var currentCommand = "";
    cmdStr += " ";
    cmdStr.toUpperCase();
    for (var letter = 0; letter < cmdStr.length; letter++) {
        if (CheckIfNumber(cmdStr[letter])) { //If the letter is a coord
            var c = {
                x: 0,
                y: cmdStr[letter + 2],
                direction: "",
                command: ""
            };
            c.x = parseInt(cmdStr[letter]);
            c.y = parseInt(cmdStr[letter + 2]);
            savedCommands.push(c);
            letter += 3;
        }
        else if (CheckIfDirection(cmdStr[letter])) {
            for (var s = savedCommands.length - 1; s > -1; s--) {
                if (savedCommands[s].direction == "") {
                    savedCommands[s].direction = cmdStr[letter];          
                }
                else {
                    break;
                }
            }
            letter++;
        }
        else if (cmdStr[letter] == " " && currentCommand != "")  {
            for (var s = savedCommands.length - 1; s > -1; s--) {
                if (savedCommands[s].command == "") {
                    savedCommands[s].command = currentCommand;
                }
            }
            currentCommand = "";
        }
        else {
            currentCommand += cmdStr[letter];
        }
    }
    return savedCommands;
}