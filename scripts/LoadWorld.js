function LoadWorldObjects(loader, scene) {
	loader.load('models/floor.glb',
		function (gltf) {
			var floor = {
				model: gltf.scene
			}
			floor.model.position.set(6 ,-5 ,6);
			scene.add(floor.model);
		}
	);

	loader.load('models/skybox.glb',
		function (gltf) {
			var sky = {
				model: gltf.scene
			}
			sky.model.position.set(6 ,-10 ,6);
			scene.add(sky.model);
		}
    );
    
	loader.load('models/bigN.glb',
		function (gltf) {
			var w = {
				model: gltf.scene
			}
			w.model.position.set(25 ,3 ,6);
			scene.add(w.model);
		}
    );

}

