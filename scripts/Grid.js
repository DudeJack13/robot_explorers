var grid = [];
var lavaGrid = [];
var gridObjectsToLoad = 0;
var gridObjectsLoaded = 0;
var loader, scene;
const size = 2.5;
var gridSizeX, gridSizeY;

function GridStartUp(loader, scene) {
    this.loader = loader;
    this.scene = scene;
}

function CreateGrid(xLength, yLength) {
    gridSizeX = xLength;
    gridSizeY = yLength;
    for (var y = 0; y < yLength; y++) {
        for (var x = 0; x < xLength; x++) {
            LoadSquare(x ,y);
        }
    }
    for (var y = 0; y < yLength; y++) {
        LoadLava(x ,y);
        LoadLava(x - gridSizeX - 1 ,y);
    }
    for (var x = 0; x < xLength; x++) {
        LoadLava(x ,y);
        LoadLava(x ,y - gridSizeY - 1);
    }
    
}

function LoadSquare(x, y) {
    gridObjectsToLoad++;
    loader.load('models/stone_square.glb',
        function (gltf) {
            var rS = {
                gridX: x,
                gridY: y,
                middle: 0,
                model: gltf.scene,
            };
            var rand = Math.floor(Math.random() * 2) + 0; 
            rS.model.position.set(x * size, rand ,y * size);
            scene.add(rS.model);
            grid.push(rS);
            gridObjectsLoaded++; 
        }
    );
}

function LoadLava(x, y) {
    gridObjectsToLoad++;
    loader.load('models/lava_square.glb',
        function (gltf) {
            var lava = {
                gridX: x,
                gridY: y,
                model: gltf.scene,
            };
            lava.model.position.set(x * size, -2 ,y * size);
            scene.add(lava.model);
            lavaGrid.push(lava);
            gridObjectsLoaded++; 
        }
    );
}

var GetSquareWorldPos = function(gridY, gridX) {
    for (var i = 0; i < grid.length; i++) {
        if (grid[i].gridX == gridX && grid[i].gridY == gridY) {
            return new THREE.Vector3(grid[i].model.position.x, grid[i].model.position.y, grid[i].model.position.z);
        }
    }
    return null;
}
