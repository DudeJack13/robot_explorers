var lights = [];

var CreatePointLight = function(scene, x, y, z, colour, intensity) {
    var l = new THREE.PointLight(colour, intensity, 0);
    l.position.set(x, y, z);
    l.lookAt(6, 0, 6);
    scene.add(l);
    lights.push(l);
    return lights[lights.length - 1];
}