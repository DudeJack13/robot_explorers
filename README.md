# Extra Information #
This has been tested and working on Edge

In the email, the test input was " 5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM". I was unsure if the 5,5 was suppose to use the following N or default spawn at N. I decided to use the N for both 1,2 and 5,5.

The code for the Phaser that converts the input for the robots is contained in "CommandPhaser"

The game engine used is ThreeJS and everything has been written in Java Script.

When a robot falls out grid, it will fall off out the world while spinning. When a robot has finished it's commands, it will begin to spin. Use the mouse to rotate the camera around.